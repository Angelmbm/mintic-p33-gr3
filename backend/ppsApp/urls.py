from django.urls import path
from ppsApp import views

urlpatterns = [
    path('categories/', views.CategoriesList.as_view()),
    path("category/<int:pk>/", views.CategoryDetail.as_view()),
    path('category/add/', views.CategoryAdd.as_view()),
    path("category/update/<int:pk>/", views.CategoryUpdate.as_view()),
    path("category/delete/<int:pk>/", views.CategoryDelete.as_view()),
]