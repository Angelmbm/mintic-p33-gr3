from django.contrib import admin

# Register your models here.
from .models.user import User
from .models.vendedor import Vendedor
from .models.producto import Producto
from .models.category import Category
from .models.pedidos import Pedido
from .models.comprador import Comprador

admin.site.register(User)
admin.site.register(Vendedor)
admin.site.register(Producto)
admin.site.register(Category)
admin.site.register(Comprador)
admin.site.register(Pedido)
