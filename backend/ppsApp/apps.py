from django.apps import AppConfig


class PpsappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ppsApp'
