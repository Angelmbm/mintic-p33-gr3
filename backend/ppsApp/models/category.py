from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=120)
    slug = models.SlugField()
    