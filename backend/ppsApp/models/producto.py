from django.db import models
from .vendedor import Vendedor
class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    vendedor = models.ForeignKey(Vendedor, related_name='vendedor_producto', on_delete=models.CASCADE)
    prod_name = models.CharField('Nombre Producto', max_length = 100)
    prod_price = models.IntegerField(default=0)
    prod_weight = models.IntegerField(default=0)
    prod_date = models.DateTimeField()
    prod_desc = models.CharField('Descripción Producto', max_length = 300)
    prod_type = models.CharField('Tipo Producto', max_length = 500)
    prod_stock = models.BooleanField(default=True)
    prod_Quanty = models.IntegerField(default=0)