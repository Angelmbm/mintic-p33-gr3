from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password
from .user import User

class Comprador(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField('Nombre', max_length = 150)
    phone = models.IntegerField('Phone',default=0,null=True)
    address = models.CharField('Dirección', max_length = 50)
    id_user = models.ForeignKey(User, on_delete= models.CASCADE, related_name="comprador")
    isActive = models.BooleanField(default=True)