from django.db import models
from .user import User

class Vendedor(models.Model):
    id = models.BigAutoField(primary_key=True)
    name_store = models.CharField('Nombre Tienda', max_length = 150)
    phone = models.IntegerField('Phone',default=0,null=True)
    rut = models.IntegerField(default=0)
    address = models.CharField('Dirección', max_length = 50)
    user = models.ForeignKey(User, on_delete= models.CASCADE, related_name="vendedor")
    isActive = models.BooleanField(default=True)