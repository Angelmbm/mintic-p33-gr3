from django.db import models
from .comprador import Comprador
from .producto import Producto

class Pedido(models.Model):
    id = models.AutoField(primary_key=True)
    id_comprador=models.ForeignKey(Comprador, related_name='comprador', on_delete=models.CASCADE)
    id_producto=models.ForeignKey(Producto, related_name='producto', on_delete=models.CASCADE)
    prod_Quanty = models.IntegerField(default=0)
    pedido_date = models.DateTimeField()