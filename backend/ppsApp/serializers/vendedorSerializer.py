from rest_framework import serializers
from ppsApp.models.vendedor import Vendedor
from ppsApp.models.producto import Producto
from ppsApp.serializers.productoSerializer import ProductoSerializer
class VendedorSerializer(serializers.ModelSerializer):

	class Meta:
		model = Vendedor
		fields = ['id', 'name_store', 'phone', 'rut', 'address', 'isActive', 'user_id']
	def create(self, validated_data):
		vendedorInstance = Vendedor.objects.create(**validated_data)
		return vendedorInstance
	def to_representation(self, obj):
		vendedor = Vendedor.objects.get(id=obj.id)
		return {
			'id': vendedor.id,
            'name_store':vendedor.name_store,
            'phone': vendedor.phone,
            'rut': vendedor.rut,
            'address': vendedor.address,
            'isActive': vendedor.isActive,
			'user_id': vendedor.user_id,
			#print(vendedor.count(vendedor.user_id)),
		}