from rest_framework import serializers
from ppsApp.models.comprador import Comprador
from ppsApp.models.user import User

class CompradorSerializer(serializers.ModelSerializer):

	class Meta:
		model = Comprador
		fields = ['id', 'name', 'phone', 'address', 'id_user', 'isActive']
	def create(self, validated_data):
		compradorInstance = Comprador.objects.create(**validated_data)
		return compradorInstance
	def to_representation(self, obj):
		comprador = Comprador.objects.get(id=obj.id)
		return {
			'id': comprador.id,
            'name':comprador.name,
            'phone': comprador.phone,
            'address': comprador.address,
            'id_user': comprador.id_user,
            'isActive': comprador.isActive,
		}