from ppsApp.models.producto import Producto
from rest_framework import serializers
class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['prod_name', 'prod_price', 'prod_weight', 'prod_date', 'prod_desc', 'prod_type', 'prod_stock', 'prod_Quanty']