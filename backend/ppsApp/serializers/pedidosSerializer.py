from ppsApp.models.pedidos import Pedido
from rest_framework import serializers
class PedidosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pedido
        fields = ['id_comprador', 'id_producto', 'prod_Quanty', 'pedido_date']