# Generated by Django 3.2.8 on 2021-10-11 00:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ppsApp', '0002_auto_20211009_1653'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vendedor',
            old_name='id_user',
            new_name='user',
        ),
        migrations.AlterField(
            model_name='user',
            name='cellphone',
            field=models.IntegerField(verbose_name='Cellphone'),
        ),
        migrations.AlterField(
            model_name='user',
            name='roles',
            field=models.CharField(choices=[('V', 'Vendedor'), ('C', 'Comprador'), ('A', 'administrador')], default='C', max_length=1),
        ),
    ]
