from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import *
from rest_framework import status

from ppsApp.models.producto import Producto
from ppsApp.models.vendedor import Vendedor
from ppsApp.serializers.productoSerializer import ProductoSerializer
class ProductosDetailView(ListAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer

class ProductoDetailView(APIView):

    def get_producto(self, pk):
        try:
            return Producto.objects.get(pk=pk)
        except Producto.DoesNotExist:
            raise status.HTTP_404_Not_Found

    def get(self, request, pk, format=None):
        query = self.get_producto(pk)
        serializer = ProductoSerializer(query)
        return Response(serializer.data)

class ProductoCreateView(APIView):

    def post(self, request, format=None):
        serializer0 = ProductoSerializer(data=request.data)
        if serializer0.is_valid():
            serializer0.save()
            return Response(serializer0.data, status=status.HTTP_201_CREATED)
        return Response(serializer0.errors, status=status.HTTP_400_BAD_REQUEST)

class ProductoUpdateView(APIView):

    def get_producto0(self, pk):
        try:
            return Producto.objects.get(pk=pk)
        except Producto.DoesNotExist:
            raise status.HTTP_404_Not_Found

    def put(self, request, pk, format=None):
        query0 = self.get_producto0(pk)
        serializer00 = ProductoSerializer(query0, data=request.data)
        if serializer00.is_valid():
            serializer00.save()
            return Response(serializer00.data)
        return Response(serializer00.errors, status=status.HTTP_400_BAD_REQUEST)

class ProductoDeleteView(APIView):

    def get_producto00(self, pk):
        try:
            return Producto.objects.get(pk=pk)
        except Producto.DoesNotExist:
            raise status.HTTP_404_Not_Found

    def delete(self, request, pk, format=None):
        query00 = self.get_producto00(pk)
        query00.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)