from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import *
from rest_framework import status

from ppsApp.models.user import User
from ppsApp.models.vendedor import Vendedor
from ppsApp.serializers.vendedorSerializer import VendedorSerializer

class VendedoresDetailView(ListAPIView):
    queryset = Vendedor.objects.all()
    serializer_class = VendedorSerializer
    
class VendedorDetailView(APIView):

    def get_vendedor(self, pk):
        try:
            return Vendedor.objects.get(pk=pk)
        except Vendedor.DoesNotExist:
            raise HTTP_404_Not_Found

    def get(self, request, pk, format=None):
        query = self.get_vendedor(pk)
        serializer = VendedorSerializer(query)
        return Response(serializer.data)

class VendedorUpdateView(APIView):

    def get_vendedor0(self, pk):
        try:
            return Vendedor.objects.get(pk=pk)
        except Vendedor.DoesNotExist:
            raise HTTP_404_Not_Found

    def put(self, request, pk, format=None):
        query0 = self.get_vendedor0(pk)
        serializer00 = VendedorSerializer(query0, data=request.data)
        if serializer00.is_valid():
            serializer00.save()
            return Response(serializer00.data)
        return Response(serializer00.errors, status=status.HTTP_400_BAD_REQUEST)

class VendedorDeleteView(APIView):

    def get_vendedor00(self, pk):
        try:
            return Vendedor.objects.get(pk=pk)
        except Vendedor.DoesNotExist:
            raise HTTP_404_Not_Found

    def delete(self, request, pk, format=None):
        query00 = self.get_vendedor00(pk)
        query00.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)