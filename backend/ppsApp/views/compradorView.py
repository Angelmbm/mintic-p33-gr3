from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import *
from rest_framework import status

from ppsApp.models.user import User
from ppsApp.models.comprador import Comprador
from ppsApp.serializers.compradorSerializer import CompradorSerializer

class CompradoresDetailView(ListAPIView):
    queryset = Comprador.objects.all()
    serializer_class = CompradorSerializer

class CompradorDetailView(APIView):

    def get_comprador(self, pk):
        try:
            return Comprador.objects.get(pk=pk)
        except Comprador.DoesNotExist:
            raise HTTP_404_Not_Found

    def get(self, request, pk, format=None):
        query = self.get_comprador(pk)
        serializer = CompradorSerializer(query)
        return Response(serializer.data)

class CompradorCreateView(APIView):

    def post(self, request, format=None):
        serializer0 = CompradorSerializer(data=request.data)
        if serializer0.is_valid():
            serializer0.save()
            return Response(serializer0.data, status=status.HTTP_201_CREATED)
        return Response(serializer0.errors, status=status.HTTP_400_BAD_REQUEST)

class CompradorUpdateView(APIView):

    def get_comprador0(self, pk):
        try:
            return Comprador.objects.get(pk=pk)
        except Comprador.DoesNotExist:
            raise HTTP_404_Not_Found

    def put(self, request, pk, format=None):
        query0 = self.get_comprador0(pk)
        serializer00 = CompradorSerializer(query0, data=request.data)
        if serializer00.is_valid():
            serializer00.save()
            return Response(serializer00.data)
        return Response(serializer00.errors, status=status.HTTP_400_BAD_REQUEST)

class CompradorDeleteView(APIView):

    def get_comprador00(self, pk):
        try:
            return Comprador.objects.get(pk=pk)
        except Comprador.DoesNotExist:
            raise HTTP_404_Not_Found

    def delete(self, request, pk, format=None):
        query00 = self.get_comprador00(pk)
        query00.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)