from rest_framework import status, views
from rest_framework.response import Response
from rest_framework.generics import *
from rest_framework.views import APIView

from ppsApp.models.user import User
from ppsApp.models.vendedor import Vendedor
from ppsApp.serializers.vendedorSerializer import VendedorSerializer

class VendedorCreateView(views.APIView):
    
    def post(self, request,  format=None):
        serializer = VendedorSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)