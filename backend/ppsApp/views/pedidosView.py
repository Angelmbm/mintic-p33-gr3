from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import *
from rest_framework import status

from ppsApp.models.producto import Producto
from ppsApp.models.comprador import Comprador
from ppsApp.serializers.pedidosSerializer import PedidosSerializer
from ppsApp.models.pedidos import Pedido

class PedidosDetailView(ListAPIView):
    queryset = Pedido.objects.all()
    serializer_class = PedidosSerializer

class PedidoDetailView(APIView):

    def get_pedido(self, pk):
        try:
            return Pedido.objects.get(pk=pk)
        except Pedido.DoesNotExist:
            raise status.HTTP_404_Not_Found

    def get(self, request, pk, format=None):
        query = self.get_pedido(pk)
        serializer = PedidosSerializer(query)
        return Response(serializer.data)

class PedidoCreateView(APIView):

    def post(self, request, format=None):
        serializer0 = PedidosSerializer(data=request.data)
        if serializer0.is_valid():
            serializer0.save()
            return Response(serializer0.data, status=status.HTTP_201_CREATED)
        return Response(serializer0.errors, status=status.HTTP_400_BAD_REQUEST)

class PedidoUpdateView(APIView):

    def get_pedido0(self, pk):
        try:
            return Pedido.objects.get(pk=pk)
        except Pedido.DoesNotExist:
            raise status.HTTP_404_Not_Found

    def put(self, request, pk, format=None):
        query0 = self.get_pedido0(pk)
        serializer00 = PedidosSerializer(query0, data=request.data)
        if serializer00.is_valid():
            serializer00.save()
            return Response(serializer00.data)
        return Response(serializer00.errors, status=status.HTTP_400_BAD_REQUEST)

class PedidoDeleteView(APIView):

    def get_pedido00(self, pk):
        try:
            return Pedido.objects.get(pk=pk)
        except Pedido.DoesNotExist:
            raise status.HTTP_404_Not_Found

    def delete(self, request, pk, format=None):
        query00 = self.get_pedido00(pk)
        query00.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)