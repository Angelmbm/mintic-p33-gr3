from django.urls import path, include
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from ppsApp import views
urlpatterns = [
path('login/', TokenObtainPairView.as_view()),
path('refresh/', TokenRefreshView.as_view()),
path('new_user/', views.UserCreateView.as_view()),
path('user/<int:pk>/', views.UserDetailView.as_view()),
path('edit/<int:pk>/', views.UserUpdateView.as_view()),
path('delete/<int:pk>/', views.UserDeleteView.as_view()),
path('user/<int:pk>/', views.UserDetailView.as_view()),
path('', include('ppsApp.urls')),
path('productos/', views.ProductosDetailView.as_view()),
path('producto/<int:pk>/', views.ProductoDetailView.as_view()),
path('producto/add/', views.ProductoCreateView.as_view()),
path("producto/update/<int:pk>/", views.ProductoUpdateView.as_view()),
path("producto/delete/<int:pk>/", views.ProductoDeleteView.as_view()),
path('vendedores/', views.VendedoresDetailView.as_view()),
path('vendedor/<int:pk>/', views.VendedorDetailView.as_view()),
path('vendedor/add/', views.VendedorCreateView.as_view()),
path("vendedor/update/<int:pk>/", views.VendedorUpdateView.as_view()),
path("vendedor/delete/<int:pk>/", views.VendedorDeleteView.as_view()),
path('pedidos/', views.PedidosDetailView.as_view()),
path('pedido/<int:pk>/', views.PedidoDetailView.as_view()),
path('pedido/add/', views.PedidoCreateView.as_view()),
path("pedido/update/<int:pk>/", views.PedidoUpdateView.as_view()),
path("pedido/delete/<int:pk>/", views.PedidoDeleteView.as_view()),
path('compradores/', views.CompradoresDetailView.as_view()),
path('comprador/<int:pk>/', views.CompradorDetailView.as_view()),
path('comprador/add/', views.CompradorCreateView.as_view()),
path("comprador/update/<int:pk>/", views.CompradorUpdateView.as_view()),
path("comprador/delete/<int:pk>/", views.CompradorDeleteView.as_view()),
]