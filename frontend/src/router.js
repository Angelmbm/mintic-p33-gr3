import { createRouter, createWebHistory } from "vue-router";
import App from './App.vue';

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'

import Profile from './components/Profile.vue'
import UpdateProfile from './components/UpdateProfile.vue'
import CreateProducts from './components/CreateProducts.vue'
import Products from './components/Products.vue'




const routes = [{
        path: '/',
        name: 'root',
        component: App
    },
    {
        path: '/user/logIn',
        name: "logIn",
        component: LogIn
    },
    {
        path: '/user/signUp',
        name: "signUp",
        component: SignUp
    },
    {
        path: '/user/home',
        name: "home",
        component: Home
    },
    {
        path: '/user/profile',
        name: "profile",
        component: Profile
    },
    {
        path: '/user/update',
        name: "updateProfile",
        component: UpdateProfile
    },
    {
        path: '/user/createProducts',
        name: "createProducts",
        component: CreateProducts
    },
    {
        path: '/user/products',
        name: "products",
        component: Products
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;